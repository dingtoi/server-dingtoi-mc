module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      host: 'dingtoi.com',
      user: 'dingtoi',
      password: 'pass',
      database: 'dingtoi',
      port: 30082
    },
    /*pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }*/
  },

  test: {
    client: 'postgresql',
    connection: {
      host: 'dingtoi.com',
      user: 'dingtoi',
      password: 'pass',
      database: 'dingtoi',
      port: 30082
    },
    /*pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }*/
  },

  production: {
    client: 'postgresql',
    connection: {
      host: 'dingtoi.com',
      user: 'dingtoi',
      password: 'pass',
      database: 'dingtoi',
      port: 30082
    },
    /*pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }*/
  }
}

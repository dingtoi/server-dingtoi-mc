const express = require('express');

const db = require('../db');

const router = express.Router();

router.post('/report', (req, res) => {
  const { code, sel, auto } = req.body;
  if (code && sel && auto) {
    try {
      const { gra, dia } = JSON.parse(sel);
      if (!gra) {
        res.status(500).json({ err: 'Param SEL has problems' });
        return false;
      }
      try {
        const { dev, tou, pro, os, stru, str, cam, fla, vol, spk, wi, blu, fin, vco, vci, txo, txi, dado, daup } = JSON.parse(auto);
        if (!dev || !tou || !pro || !os || !stru || !str
          || !cam || !fla || !vol || !spk || !wi || !blu || !fin || !vco || !vci || !txo || !txi || !dado || !daup) {
          res.status(500).json({ err: 'Param AUTO has problems' });
          return false;
        }
        db.checkTransaction(code)
          .then((transRow) => {
            if (!transRow) {
              res.status(400).json({ err: 'Transaction Not Exists' });
              return false;
            }
            res.status(200).json({
              isSuccess: true,
              response: null
            });
          })
          .catch(err => {
            res.status(500).json('DATABASE ERROR: ' + err.message);
          });
      }
      catch (e) {
        res.status(500).json({ err: 'Param AUTO has problems: ' + e });
        return false;
      }
    } catch (e) {
      res.status(500).json({ err: 'Param SEL has problems: ' + e });
      return false;
    }

  } else {
    res.status(500).json({ err: 'Params failed' });
    return false;
  }
});

module.exports = router;

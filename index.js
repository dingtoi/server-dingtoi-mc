const server = require('./server');

const port = process.env.PORT || 30083;

server.listen(port, () => {
  console.log('Listening on port', port);
});

const environment = process.env.NODE_ENV || 'development';
const config = require('./knexfile')[environment];
const connection = require('knex')(config);

module.exports = {
  checkTransaction: checkTransaction
}

function checkTransaction(code, db = connection) {
  return db('transactions').select('id').where({ 'transactionCode': code }).first();
}
